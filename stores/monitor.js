module.exports = function(state, emitter) {
  state.monitor = {
    latest: {},
    messages: [],
    elements: []
  };

  emitter.on("mqtt:connected", sub);

  function sub() {
    if(state.route === "monitor") {
      state.mqtt.enableMonitor(true);
      state.mqtt.monitor.on("message", handleMessage);
    }

    emitter.on("navigate", function() {
      if(state.route === "monitor") {
        state.mqtt.enableMonitor(true);
        state.mqtt.monitor.on("message", handleMessage);
      } else {
        state.mqtt.enableMonitor(false);
        state.mqtt.monitor.removeListener("message", handleMessage);
      }
    });
  }

  function handleMessage(message) {
    state.monitor.latest[message.topic] = message;
    state.monitor.messages.push(message);

    emitter.emit("render");
  }
}
