var config = require("../config");
var level = require("level-browserify");
var path = require("path");
var sublevel = require("level-sublevel");

module.exports = function(state, emitter) {
  var dbPath = path.join(__dirname, config.db.relPath);

  var db = sublevel(level(dbPath), {
    valueEncoding: "json"
  });

  state.db = db;
  state.subscriptions = [];

  var subscriptions = db.sublevel("subscriptions");

  emitter.on("mqtt:connected", function() {
    /*
     * uncontroled replay of all content turned out to be a bad idea.
    subscriptions.createReadStream().on("data", function(entry) {
      emitter.emit("subscriptions:new", entry.value);
    });
    */
  });

  emitter.on("subscriptions:new", function(subscription) {
    state.subscriptions.push(subscription);
    emitter.emit("render");
  });

  emitter.on("subscriptions:add", function(subscription) {
    subscriptions.put(Date.now(), subscription, function(err) {
      if(err) return console.error(err);

      emitter.emit("subscriptions:new", subscription);
      emitter.emit("render");
    });
  });
};
