var EventEmitter = require("events").EventEmitter;
var config = require("../config");
var paho = require("paho.mqtt.js");

module.exports = function(state, emitter) {
  var messageDB = state.db.sublevel("messages");
  var ee = new EventEmitter();

  /*
  messageDB.createReadStream().on("data", function(entry) {
    var payload = entry.value;

    messageDB.put(Date.now(), payload);

    ee.emit(payload.topic, payload);

    ee.monitor.emit("message", payload);

    if(!state.messages[payload.topic]) {
      state.messages[payload.topic] = [];
    }

    state.messages[payload.topic].push(payload);
  });
  */

  emitter.on("db:client", function(client) {
    var connection = new paho.Client(client.host, client.port || 5679, client.clientId + Date.now());

    state.clients[client.name].connection = connection;

    ee.monitor = new EventEmitter();
    ee.connected = false;

    state.messages = {};
    state.mqtt = ee;
    state.mqttReconnectTimeout = 3000;
    state.mqtt.connections = [];

    connection.onConnectionLost = function(err) {
      console.log("i died", typeof err, err);

      setTimeout(connect, state.mqttReconnectTimeout);

      ee.connected = false;
    };

    ee.enableMonitor = function(enabled) {
      if(enabled === true || enabled === undefined)
        subscribe("#");
      else
        unsubscribe("#");
    }

    connection.onMessageArrived = function(message) {
      //console.log(message.destinationName, message.payloadString);
      var payload = {
        ts: new Date(),
        payload: message.payloadString,
        topic: message.destinationName
      };

      messageDB.put(Date.now(), payload);

      ee.emit(message.destinationName, payload);

      ee.monitor.emit("message", payload);

      if(!state.messages[message.destinationName]) {
        state.messages[message.destinationName] = [];
      }

      state.messages[message.destinationName].push(payload);
    };

    connect();

    function connect() {
      console.log("connecting");

      var connectionConfig = {
        onSuccess: function() {
          console.log("mqtt connected", client.host);

          ee.connected = true;

          emitter.emit("mqtt:connected");
          emitter.on("subscriptions:new", function(sub) {
            subscribe(sub.topic);
          });
        }
      };

    if(client.username)
      connectionConfig.userName = client.username;

    if(client.password)
      connectionConfig.password = client.password;
      connection.connect(connectionConfig);
    }

    function subscribe(topic) {
      console.log("mqtt subscribing: %s", topic);

      connection.subscribe(topic);
    }

    function unsubscribe(topic) {
      console.log("mqtt unsubscribing: %s", topic);

      connection.unsubscribe(topic);
    }
  });
}
