module.exports = function(state, emitter) {
  var db = state.db.sublevel("clients");

  state.clients = {};

  console.log("store/client fetching client DB");

  db.createReadStream().on("data", function(entry) {
    console.log(entry);

    var data = entry.value;

    state.clients[data.name] = data;

    emitter.emit("db:client", data);
  }).on("end", function() {
    emitter.emit("render");
  });

  emitter.on("client:add", function(data) {
    console.log("client:add storing client in db");

    db.put(data.name, data, function(err) {
      if(err) throw err;

      state.clients[data.name] = data;
      emitter.emit("db:client", data);
    });
  });
};
