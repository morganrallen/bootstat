var config = require("../config");

config.db.relPath = "../test/db";

var test = require("tape");
var db = require("../stores/db");
var EventEmitter = require("events").EventEmitter;

test(function(t) {
  t.plan(5);

  var state = {};
  var emitter = new EventEmitter();

  var sub = {
    topic: "/test"
  };

  db(state, emitter);

  t.equal(state.subscriptions.length, 0, "db subscriptions array initialized");

  var renderCount = 0;
  emitter.on("render", function() {
    ++renderCount;
  });

  emitter.emit("subscriptions:new", sub);

  t.equal(state.subscriptions.length, 1, "subscriptions:new add subscription");
  t.equal(renderCount, 1, "subscription:new triggered render");

  emitter.emit("subscriptions:add", sub);

  setTimeout(function() {
    t.equal(state.subscriptions.length, 2, "subscriptions:add add subscription");
    t.equal(renderCount, 3, "subscription:add and subscriptions:new each triggered renders");
  }, 50);
}, "subscription database");
