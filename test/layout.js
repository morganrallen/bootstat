var choo = require("choo");
var html = require("choo/html");
var test = require("tape");

test("Rendering layout", function(t) {
  var layout = require("../views/layout");
  var app = choo();
  var str = "Im inside the other thing";
  var markup = html`<div>${str}</div>`;

  app.route("/string", function() {
    return layout(str);
  });

  app.route("/html", function() {
    return layout(markup);
  });

  t.ok(app.toString("/string").indexOf(str) !== -1, "got string in output");
  t.ok(app.toString("/html").indexOf(markup.toString()) !== -1, "got HTML in output");

  layout.setState({
    route: "/"
  });

  t.ok(app.toString("/string").indexOf("mr3 underline\" href=\"/\"") !== -1, "Home got underline class");

  t.end();
});
