bootstat
--------
MQTT dashboard

Require MQTT broker with WebSockets enabled. For Mosquitto can be as simple as adding this to config.
```
listener 5679 127.0.0.1
protocol websockets
```

```
npm install
npm run dev
```

* Use `(+)` in menu to add new client connectiong
* `Monitor` will show all incoming messages.
* Use `[+]` in `Monitor` to add subscription
* Charts will be displayed in `Home`
