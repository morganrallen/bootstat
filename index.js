var css = require("sheetify");
var app = require("choo")();
var config = require("./config");
var Chart = require("chart.js");
var html = require("choo/html");
var layout = require("./views/layout");
var request = require("browser-request");
var xtend = require("xtend");

window.app = app;

var appLayout = [{
  name: "Light Level",
  state: {
    channel: "/shop/light",
    labels: [],
    datasets: [{
      label: "Light",
      data: []
    }]
  },
  type: "line",
}, {
  state: {
    channel: "cpu",
    labels: [],
    datasets: [{
      label: "CPU",
      data: []
    }]
  }
}];

css("tachyons");

app.use(function(state) {
  layout.setState(state);
});

app.use(function(state, emitter) {
  emitter.emit("DOMTitleChange", "boot");
});

app.use(require("./stores/db"));

app.use(require("./stores/clients"));
app.use(require("./stores/mqtt"));
app.use(require("./stores/monitor"));
app.use(require("./stores/subscriptions"));

var routeAdd = require("./routes/add");
app.route(`${config.baseURL}/add`, routeAdd);
app.route(`${config.baseURL}/add/*`, routeAdd);

var routeSubs = require("./routes/subscriptions");
app.route(`${config.baseURL}/subscriptions`, routeSubs);
app.route(`${config.baseURL}/subscriptions/*`, routeSubs);

app.route(`${config.baseURL}/monitor`, require("./routes/monitor"));

app.route(`${config.baseURL}/addclient`, require("./routes/addclient"));

app.route(`${config.baseURL}/`, require("./routes/dashboard"));

console.log("mounting choo app");

app.mount(document.body);
