var html = require("choo/html");

// from feather-icons, unfortunately doesn't play well with nanohtml
// due to issues with self-closing tags
var iconPlus = html`
  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
       viewBox="0 0 24 24" fill="none" stroke="currentColor"
       stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
       class="feather feather-plus-circle"
  >
    <circle cx="12" cy="12" r="10"/>
    <line x1="12" y1="8" x2="12" y2="16"/>
    <line x1="8" y1="12" x2="16" y2="12"/>
  </svg>`;

var state = {};

module.exports = function layout(inner) {
  return html`<body>
    <header class="bg-black-90 w-100 ph3 pv3 pv4-ns ph4-m ph5-l">
      <nav class="f6 fw6 ttu tracked">
        <a class="link dim white dib mr3 ${isSelected("/")}" href="/" title="Home">Home</a>
        <a class="link dim white dib mr3 ${isSelected("add")}" href="/add" title="Add">Add</a>
        <a class="link dim white dib mr3 ${isSelected("subscriptions")}" href="/subscriptions" title="Subscriptions">Subscriptions</a>
        <a class="link dim white dib mr3 ${isSelected("monitor")}" href="/monitor" title="Monitor">Monitor</a>
        <a class="link dim white dib mr3 ${isSelected("addclient")}" href="/addclient" title="Add Client">${iconPlus}</a>
        <a class="link dim white dib mr3 cl fr" href="https://gitlab.com/morganrallen/bootstat" target="_blank" title="GitLab Repo">Repo</a>
      </nav>
    </header>

    <main class="w-100 cf helvetica dark-gray bg-white pa3 pa4-m pa5-l mw9 center">
      ${inner}
    </main>
  </body>`;

  function isSelected(route) {
    if("route" in state && state.route === route) return "underline";
    return "";
  }
};

module.exports.setState = function(s) {
  state = s;
}

