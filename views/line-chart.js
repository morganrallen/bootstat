var html = require("choo/html");
var xtend = require("xtend");

const MAX_LIMIT = 10;

module.exports = function(state, emit, sub) {
  var canvas = document.createElement("canvas");
  var ctx = canvas.getContext("2d");

  var labels = [];
  var datasets = [{
    label: sub.alias || sub.topic,
    data: []
  }];

  var lineChart = new Chart(ctx, {
    type: "line",
    data: {
      labels: labels,
      datasets: datasets
    },
    options: {
      scales: {
        xAxes: [{
          type: "time",
          time: {
            //unit: "day"
          },
          display: true,
          scaleLabel: {
            display: true,
            labelString: "Time"
          }
        }],
        yAxes: [{
          display: true,
          ticks: {
          },
          scaleLabel: {
            display: true,
            labelString: sub.alias
          }
        }]
      }
    }
  });

  if(state.messages[sub.topic]) {
    state.messages[sub.topic].forEach(processMessage);
  };

  state.mqtt.on(sub.topic, processMessage);

  function processMessage(message) {
    labels.push(Date.now());
    datasets[0].data.push(message.payload);

    if(labels.length > (sub.limit || MAX_LIMIT)) {
      labels.shift();
      datasets[0].data.shift();
    }

    lineChart.update();
  };

  console.log(sub);

  return canvas;
};
