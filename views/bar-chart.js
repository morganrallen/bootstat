var html = require("choo/html");
var xtend = require("xtend");

module.exports = function(state, emit, sub) {
  var canvas = document.createElement("canvas");
  var ctx = canvas.getContext("2d");

  var startHour = (new Date).getHours();

  var labels = Array.from({length: 12}).map(function(v, i) {
    return `${startHour - i}:00:00`;
  }).reverse();

  var data = Array.from({length: 12}).map(function(v, i) {
    return 0;
  });

  var barChart = new Chart(ctx, {
    type: "bar",
    data: {
      labels: labels,
      datasets: [{
        data: data
      }]
    }
  });

  if(state.messages[sub.topic]) {
    state.messages[sub.topic].forEach(processPayload);
  };

  state.mqtt.on(sub.topic, processPayload);

  function processPayload(payload) {
    var hour = payload.ts.getHours();
    var now = (new Date).getHours();

    if(now !== startHour) {
      startHour = now;
      labels.shift();
      data.shift();

      labels[11] = `${now}:00:00`;
      data[11] = 0;
    }

    data[11]++;

    barChart.update();
  }

  return canvas;
}
