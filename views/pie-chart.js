var html = require("choo/html");
var xtend = require("xtend");

module.exports = function(state, emit, sub) {
  var canvas = document.createElement("canvas");
  var ctx = canvas.getContext("2d");

  var labels = [];
  var data = [];

  var pieChart = new Chart(ctx, {
    type: "pie",
    data: {
      labels: labels,
      datasets: [{
        backgroundColor: [ "Red", "Green", "Blue", "Purple", "Yellow" ],
        data: data
      }]
    }
  });

  if(state.messages[sub.topic]) {
    state.messages[sub.topic].forEach(processMessage);
  };

  state.mqtt.on(sub.topic, processMessage);

  function processMessage(message) {
    var payload = message.payload;

    if(sub.filter) payload = (new Function("v", sub.filter))(payload);

    var idx = labels.indexOf(payload);
    if(idx === -1) {
      idx = labels.push(payload) - 1;
      data[idx] = 0;
    }

    data[idx]++;

    pieChart.update();
  }

  return canvas;
};

