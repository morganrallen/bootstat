var html = require("choo/html");
var layout = require("../views/layout");
var lineChart = require("../views/line-chart");
var pieChart = require("../views/pie-chart");
var barChart = require("../views/bar-chart");

module.exports = function(state, emit) {
  return layout(html`
    <div class="charts">
    ${ checkClientsSubs() }
    ${ state.subscriptions.map(function(sub) {
      return html`
        <div class="chart">
        ${chart(sub)}
        </div>
      `;
    })}
  </div>
  `);

  function checkClientsSubs() {
    if(Object.keys(state.clients).length === 0)
      return html`
        <div>
          <p>No client connections</p>
          <a href="/addclient" title="add client">Add a client</a>
        </div>
        `;
    if(state.subscriptions.length < 1)
      return html`
        <div>
          <p>No subscriptions</p>
          <a href="/add" title="add subscription">Add a subscription</a>
          <p>or</a>
          <a href="/monitor" title="monitor">View the monitor</a>
        </div>
        `;
    return "";
  }

  function chart(sub) {
    switch(sub.chartType) {
      case "line":
        return lineChart(state, emit, sub);
      case "pie":
        return pieChart(state, emit, sub);
      case "bar":
        return barChart(state, emit, sub);
    }
  }
}
