var html = require("choo/html");
var layout = require("../views/layout");

module.exports = function(state, emit) {
  if(Object.keys(state.clients).length === 0) {
    setTimeout(function() {
      emit("replaceState", "/addclient");
    }, 100);

    return layout();
  }

  return layout(html`
    <div class="subscription-list">
    <ul>
      ${state.subscriptions.map(function(msg) {
        return html`
          <li>
            <span>${msg.topic}</span>
            <span> - </span>
            <span>${ state.monitor.latest[msg.topic] }</span>
          </li>
        `;
      })}
    </ul>
    </div>
  `);
}
