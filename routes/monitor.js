var css = require("sheetify");
var html = require("choo/html");
var layout = require("../views/layout");

const MAX = 20;

var className = css`
  .message-list {
    border: 1px solid black;
    list-style: none;
    min-height: 200px;
    padding-bottom: 10px;
    padding-top: 10px;
  }

  .message-list li:nth-child(even) {
    background-color: lightGray;
  }

  .message-list .subscribe-link {
    float: right;
    padding-right: 40px;
  }
`;

module.exports = function(state, emitter) {
  if(Object.keys(state.clients).length === 0) {
    setTimeout(function() {
      emit("replaceState", "/addclient");
    }, 100);

    return layout();
  }

  return layout(html`<div>
    <div class="mt3">
      <label class="db fw6 lh-copy f6" for="client">Client</label>
      <select id="client" name="client">
        ${Object.keys(state.clients).map((c) => clientOption(state.clients[c]))}
      </select>
    </div>

    <h2>Latest message</h2>

    <ul class="message-list">
    ${Object.keys(state.monitor.latest).map(function(topic) {
      message = state.monitor.latest[topic];

      return html`
        <li>
          <span>${message.topic}</span>
          <span> - </span>
          <span>${message.payload}</span>
          <span>${ subLink(message.topic) }</span>
        </li>
      `;
    })}
    </ul>

    <h2>Log</h2>
    <ul class="message-list">
    ${state.monitor.messages.map(function(msg) {
      return html`
        <li>
          <span>${msg.topic}</span>
          <span> - </span>
          <span>${msg.payload}</span>
          <span>${ subLink(msg.topic) }</span>
        </li>
      `;
    })}
    </ul>
  </div>`);

  function clientOption(client) {
    var displayName = client.name || client.host;

    return html`<option value="${client.host}">${displayName}</option>`;
  }

  function subLink(topic) {
    return html`<a alt="subscribe" class="subscribe-link" href="/add/${topic}">+</a>`;
  }
}
