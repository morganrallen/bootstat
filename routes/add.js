var Form = require("form-to-json");
var html = require("choo/html");
var layout = require("../views/layout");

module.exports = function(state, emit) {
  var topic = state.params.wildcard;

  if(Object.keys(state.clients).length === 0) {
    setTimeout(function() {
      emit("replaceState", "/addclient");
    }, 100);

    return layout();
  }

  return layout(html`
    <form id="add-topic" action="/add" onsubmit=${handleSubmit} class="measure center">
      <fieldset id="add" class="ba b--transparent ph0 mh0">
        ${ clientBlock() }

        <div class="mt3">
          <label class="db fw6 lh-copy f6" for="topic">Topic</label>
          <input class="pa2 input-reset ba bg-transparent w-100" type="text" name="topic" value="${topic || ""}" id="topic">
        </div>

        <div class="mv3">
          <label class="db fw6 lh-copy f6" for="qos">QoS</label>
          <select id="qos" name="qos">
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
          </select>
        </div>

        <div class="mv3">
          <label class="db fw6 lh-copy f6" for="">Display name</label>
          <input class="b pa2 input-reset ba bg-transparent w-100" type="text" name="alias" id="alias">
        </div>

        <div class="mv3">
          <label class="db fw6 lh-copy f6" for="chartType">Chart Type</label>
          <select id="chartType" name="chartType">
            <option value="line">Line Chart</option>
            <option value="pie">Pie</option>
            <option value="bar">Bar</option>
          </select>
        </div>

        <div class="mv3">
          <label class="db fw6 lh-copy f6" for="filter">Filter (<small>eg: return v ${">"} 500 ? "On" : "Off"</small>)</label>
          <input class="b pa2 input-reset ba bg-transparent w-100" type="text" name="filter" id="filter">
        </div>
      </fieldset>

      <div class="">
        <input class="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib" type="submit" value="Add">
      </div>
    </form>
  `);

  function clientBlock() {
    return html`<div class="mt3">
      <label class="db fw6 lh-copy f6" for="client">Client</label>
      <select id="client" name="client">
        ${Object.keys(state.clients).map((c) => clientOption(state.clients[c]))}
      </select>
    </div>`;
  }

  function clientOption(client) {
    console.log(client);
    var displayName = client.name || client.host;

    return html`<option value="${client.host}">${displayName}</option>`;
  }

  function handleSubmit(e) {
    e.preventDefault();

    var t = e.currentTarget;
    var data = Form(e.currentTarget).toJson();

    console.log(data);

    emit("subscriptions:add", data);

    emit("pushState", "/");
  }
};
