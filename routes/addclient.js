var Form = require("form-to-json");
var html = require("choo/html");
var layout = require("../views/layout");

module.exports = function(state, emit) {
  var topic = state.params.wildcard;

  return layout(html`
    <form id="add-topic" action="/addclient" onsubmit=${handleSubmit} class="measure center">
      <fieldset id="add" class="ba b--transparent ph0 mh0">
        <div class="mt3">
          <label class="db fw6 lh-copy f6" for="serverName">Name</label>
          <input class="pa2 input-reset ba bg-transparent w-100" title="Server Name" type="text" name="name" value="" id="serverName" onkeyup=${onChange} required>
        </div>

        <div class="mt3">
          <label class="db fw6 lh-copy f6" for="clientId">Client ID</label>
          <input class="pa2 input-reset ba bg-transparent w-100" title="Server Name" type="text" name="clientId" value="" id="clientId" required>
        </div>

        <div class="mt3 fl w-50 pr2">
          <label class="db fw6 lh-copy f6" for="host">Host</label>
          <input class="pa2 input-reset ba bg-transparent w-100" type="text" name="host" value="" id="host" required>
        </div>

        <div class="mt3 fl w-50 pl2">
          <label class="db fw6 lh-copy f6" for="port">Port</label>
          <input class="pa2 input-reset ba bg-transparent w-100" type="number" name="port" value=5679 id="port" required>
        </div>

        <div class="cl pt4">
          <label class="db fw6 lh-copy f6" for="username">Username <small>(optional)</small></label>
          <input class="pa2 input-reset ba bg-transparent w-100" type="text" name="username" value="" id="username">
        </div>

        <div class="mt3">
          <label class="db fw6 lh-copy f6" for="password">Password <small>(optional)</small></label>
          <input class="pa2 input-reset ba bg-transparent w-100" type="text" name="password" value="" id="username">
        </div>
      </fieldset>

      <div class="">
        <input class="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib" type="submit" value="Add">
      </div>
    </form>
  `);

  function onChange(e) {
    var t = e.currentTarget;

    if(t.value in state.clients) {
      t.setCustomValidity("Name taken");
    } else {
      t.setCustomValidity("");
    }
  }

  function handleSubmit(e) {
    e.preventDefault();

    var t = e.currentTarget;

    var data = Form(t).toJson();
    data.port = parseInt(data.port);

    console.log(data);

    // ensure unique name? probably good idea
    emit("client:add", data);

    //emit("pushState", "/");
  }
};
